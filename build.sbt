name := "test"

version := "0.0.1"

scalaVersion := "2.10.3"

resolvers += "twttr" at "http://maven.twttr.com"

libraryDependencies ++= Seq (
    "junit" % "junit" % "4.10" % "test",
    "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test"
)
