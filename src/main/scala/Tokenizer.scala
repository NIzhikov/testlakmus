trait Token

case class EOF extends Token

trait Value[T] extends Token { def value:T }

case class StringToken(value: String) extends Value[String]
case class IntegerToken(value: Integer) extends Value[Integer]

case class PropertyToken(name: String) extends Token

trait SignToken extends Token

case class EqualSign extends SignToken
case class BiggerSign extends SignToken
case class BiggerOrEqualSign extends SignToken
case class SmallerSign extends SignToken
case class SmallerOrEqualSign extends SignToken

case class AndOperator extends Token
case class OrOperator extends Token

case class LeftBracket extends Token
case class RightBracket extends Token

////////////////////////////////////////////////////////////////////////////////
// Your ultimate goal is to provide user with a friendly expression language
// to build filters. Consider the following language grammar:
//
// <string>   ::= quoted string literal
// <integer>  ::= base-10 integer literal
// <value>    ::= <string> | <integer>
// <property> ::= "prop" "[" <string> "]"
//
// <disjunct-expr> ::= <conjunct-expr> ("||" <conjunct-expr>)*
// <conjunct-expr> ::= <atom-expr> ("&&" <atom-expr>)*
// <atom-expr>     ::= "(" <disjunct-expr> ")" | <base-expr>
// <base-expr>     ::= <property> "="  <value> |
//                     <property> ">"  <value> |
//                     <property> ">=" <value> |
//                     <property> "<"  <value> |
//                     <property> "<=" <value>
//
// This language accepts strings like
//   (ex.1) prop["country"] = "RU" || prop["country"] = "UA"`
//   (ex.2) prop["age"] >= 18 && prop["age"] < 24
class Tokenizer(expr: String) {
    private var i: Int = 0

    private def isEOF = i >= expr.length
    private def skipWhitespace = while(!isEOF && expr(i).isWhitespace) i = i+1
    private def currentChar = expr(i)
    private def read = {
        if (isEOF)
            throw new IllegalStateException("Try to read when EOF reached!")
        val c = expr(i)
        i = i + 1
        c
    }

    private def readWhile(predicate: Char => Boolean): String = {
        def doReadWhile(predicate: Char => Boolean, acc: String): String = {
            if (isEOF)
                acc
            else if (predicate(currentChar))
                doReadWhile(predicate, acc + read)
            else
                acc
        }
        doReadWhile(predicate, "")
    }

    private def isNum(ch: Char) =
        ch == '0' || ch == '1' || ch == '2' ||
        ch == '3' || ch == '4' || ch == '5' ||
        ch == '6' || ch == '7' || ch == '8' || ch == '9'

    private def doNext(): Token = {
        skipWhitespace
        if (isEOF)
            new EOF
        else if (currentChar == '"') {
            read
            val str = readWhile(x => x != '"')
            read
            new StringToken(str)
        } else if (isNum(currentChar)) {
            new IntegerToken(readWhile(isNum).toInt)
        } else if (currentChar == 'p') {
            read
            if (read != 'r' || read != 'o' || read != 'p')
                throw new IllegalStateException("Wrong keyword! Awaiting prop here.")
            skipWhitespace
            if (read != '[')
                throw new IllegalStateException("Wrong keyword! Awaiting [ here.")
            skipWhitespace
            if (read != '"')
                throw new IllegalStateException("Wrong keyword! Awaiting \" here.")
            val name = readWhile(x => x != '"')
            read
            skipWhitespace
            if (read != ']')
                throw new IllegalStateException("Wrong keyword! Awaiting ] here.")
            new PropertyToken(name)
        } else if (currentChar == '(') {
            read
            new LeftBracket
        } else if (currentChar == ')') {
            read
            new RightBracket
        } else if (currentChar == '|') {
            read
            val c = read
            if (c != '|')
                throw new IllegalStateException("Single | is not an operator!")
            new OrOperator
        } else if (currentChar == '&') {
            read
            val c = read
            if (c != '&')
                throw new IllegalStateException("Single & is not an operator!")
            new AndOperator
        } else if (currentChar == '=') {
            read
            new EqualSign
        } else if (currentChar == '<') {
            read
            if (currentChar == '=') {
                read
                new SmallerOrEqualSign
            } else
                new SmallerSign
        } else if (currentChar == '>') {
            read
            if (currentChar == '=') {
                read
                new BiggerOrEqualSign
            } else
                new BiggerSign
        } else
            throw new IllegalStateException("Unknown character!")
    }

    var current: Token = doNext()
    def next: Token = {
        val c = current
        current = doNext
        c
    }
}
