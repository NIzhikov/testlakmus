import scala.collection.immutable.Stack
import scala.collection._
import scala.util.Try

////////////////////////////////////////////////////////////////////////////////
// Imagine that an external system provides you an API to query for data.
// Datum (an item) is described by a set of key-value pairs (called properties).
// Basically, you can either run an exact query (for items matching a particular
// property value) or you can run range queries. You can also compose
// boolean expressions out of these queries.
trait Item {
  // Returns value for a given property.
  def apply(prop: String): Option[Any]
}

trait Filter {
}

// Accepts an item iff all descendant filters accept the item.
trait AndFilter extends Filter {
    def must(f: Filter)
}

// Accepts an item iff any of descendant filters accept the item.
trait OrFilter extends Filter {
    def either(f: Filter)
}

// Accepts an item iff its property value falls within the specified range.
trait RangeFilter extends Filter {
    def prop(): String
    def setLowerBound(lb: Any, inclusive: Boolean = false)
    def setUpperBound(ub: Any, inclusive: Boolean = false)
    def clearLowerBound()
    def clearUpperBound()
}

// Accepts an item iff its property value matches one of the specified values.
trait ExactFilter extends Filter {
    def prop(): String
    def add(v: Any)
}

trait FilterFactory {
    // Factory method.
    def createAndFilter(): AndFilter

    // Factory method.
    def createOrFilter(): OrFilter

    // Factory method.
    def createRangeFilter(prop: String): RangeFilter

    // Factory method.
    def createExactFilter(prop: String): ExactFilter
}

case class ExactFilterImpl(prop: String, v: Any = null) extends ExactFilter {
    val patterns: mutable.ArrayBuffer[Any] = if(v == null) mutable.ArrayBuffer() else mutable.ArrayBuffer(v)
    override def add(v: Any): Unit = patterns += v
}
case class AndFilterImpl extends AndFilter {
    val filters: mutable.ArrayBuffer[Filter] = mutable.ArrayBuffer()
    override def must(f: Filter): Unit = filters += f
}
case class OrFilterImpl extends OrFilter {
    val filters: mutable.ArrayBuffer[Filter] = mutable.ArrayBuffer()
    override def either(f: Filter): Unit = filters += f
}
case class RangeFilterImpl(prop: String,
                           var lb: Any = null, var lbInclusive: Boolean = false,
                           var ub: Any = null, var ubInclusive: Boolean = false) extends RangeFilter {
    override def setUpperBound(ub: Any, inclusive: Boolean = false): Unit =  {
        this.ub = ub
        ubInclusive = inclusive
    }
    override def setLowerBound(lb: Any, inclusive: Boolean = false): Unit = {
        this.lb = lb
        lbInclusive = inclusive
    }
    override def clearUpperBound(): Unit = ub = null
    override def clearLowerBound(): Unit = lb = null
}

trait FilterBuilder {
    ////////////////////////////////////////////////////////////////////////////////
    // Your ultimate goal is to provide user with a friendly expression language
    // to build filters. Consider the following language grammar:
    //
    // <string>   ::= quoted string literal
    // <integer>  ::= base-10 integer literal
    // <value>    ::= <string> | <integer>
    // <property> ::= "prop" "[" <string> "]"
    //
    // <disjunct-expr> ::= <conjunct-expr> ("||" <conjunct-expr>)*
    // <conjunct-expr> ::= <atom-expr> ("&&" <atom-expr>)*
    // <atom-expr>     ::= "(" <disjunct-expr> ")" | <base-expr>
    // <base-expr>     ::= <property> "="  <value> |
    //                     <property> ">"  <value> |
    //                     <property> ">=" <value> |
    //                     <property> "<"  <value> |
    //                     <property> "<=" <value>
    //
    // This language accepts strings like
    //   (ex.1) prop["country"] = "RU" || prop["country"] = "UA"`
    //   (ex.2) prop["age"] >= 18 && prop["age"] < 24
    //
    // Your goal is to write a function which takes a string and constructs
    // a corresponding filter, e. g.
    def buildFilter(expr: String, isFoldExpression: Boolean = false): Try[Filter] =  {
        val factory = new FilterFactory {// Factory method.
            override def createExactFilter(prop: String): ExactFilter = new ExactFilterImpl(prop)
            def createExactFilter(prop: String, value: Any): ExactFilter = new ExactFilterImpl(prop, value)
            override def createAndFilter(): AndFilter = new AndFilterImpl
            override def createOrFilter(): OrFilter = new OrFilterImpl
            override def createRangeFilter(prop: String): RangeFilter = new RangeFilterImpl(prop)
            def createRangeFilter(prop: String, lb: Any = null, lbInclusive: Boolean = false,
                           ub: Any = null, ubInclusive: Boolean = false): RangeFilter =
                new RangeFilterImpl(prop, lb, lbInclusive, ub, ubInclusive)
        }

        def makePropertyFilter[T](name: String, value: T, sign: Token) =
            sign match {
                case EqualSign() => factory.createExactFilter(name, value)
                case BiggerSign() => factory.createRangeFilter(name, lb = value)
                case BiggerOrEqualSign() => factory.createRangeFilter(name, lb = value, lbInclusive = true)
                case SmallerSign() => factory.createRangeFilter(name, ub = value)
                case SmallerOrEqualSign() => factory.createRangeFilter(name, ub = value, ubInclusive = true)
            }

        def baseExpr(name: String, tokenizer: Tokenizer): Filter = {
            val sign = tokenizer.next
            if (sign == EOF())
                throw new IllegalStateException("Unexpected end of input.")

            sign match {
                case EqualSign() | BiggerSign() | BiggerOrEqualSign() | SmallerSign() | SmallerOrEqualSign() => {
                    val valueToken = tokenizer.next
                    valueToken match {
                        case IntegerToken(v) => makePropertyFilter(name, v, sign)
                        case StringToken(v) => makePropertyFilter(name, v, sign)
                        case EOF() =>
                            throw new IllegalStateException("Unexpected end of input.")
                        case _ =>
                            throw new IllegalStateException("Expected value, but found. " + valueToken)
                    }
                }
                case _ => throw new IllegalStateException("Expected Sing token, but found " + sign)
            }
        }

        def atomExpr(tokenizer: Tokenizer): Filter = {
            tokenizer.next match {
                case LeftBracket() => {
                    val filter = disjunctExpr(tokenizer)
                    tokenizer.current match {
                        case RightBracket() => { tokenizer.next; filter }
                        case _ => throw new IllegalStateException("Waiting for a RightBracket, but found " + tokenizer.current)
                    }
                }
                case PropertyToken(name) => baseExpr(name, tokenizer)
                case _ => throw new IllegalStateException("Waiting for a LeftBracket or Property, but found " + tokenizer.current)
            }
        }

        def conjunctExpr(tokenizer: Tokenizer): Filter = {
            val filter = atomExpr(tokenizer)
            if (tokenizer.current == AndOperator()) {
                val andFilter = factory.createAndFilter
                andFilter.must(filter)
                while (tokenizer.current == AndOperator()) {
                    tokenizer.next
                    val f = atomExpr(tokenizer)
                    andFilter.must(f)
                }
                andFilter
            } else
                filter
        }

        def disjunctExpr(tokenizer: Tokenizer): Filter = {
            val filter = conjunctExpr(tokenizer)
            if (tokenizer.current == OrOperator()) {
                val orFilter = factory.createOrFilter
                orFilter.either(filter)
                while(tokenizer.current == OrOperator()) {
                    tokenizer.next
                    orFilter.either(conjunctExpr(tokenizer))
                }
                orFilter
            } else
                filter
        }

        def doBuildFilter(tokenizer: Tokenizer) = {
            val filter = disjunctExpr(tokenizer)
            if (tokenizer.current == EOF())
                filter
            else
                throw new IllegalStateException("Wrong symbols on expression tail")
        }

        Try(doBuildFilter(new Tokenizer(expr)))
    }

    // For (ex.1) one possible filter could be constructed in the following way:
    //   ```
    //   g = createExactFilter("country"); g.add("RU");
    //   h = createExactFilter("country"); h.add("UA");
    //   f = createOrFilter(); f.either(g); f.either(h);
    //   ```
    //
    // For (ex.2) one possible filter could be constructed in the following way:
    //   ```
    //   g = createRangeFilter("age"); g.setLowerBound(18, true);
    //   h = createRangeFilter("age"); h.setUpperBound(24, false);
    //   f = createAndFilter(); f.must(g); f.must(h);
    //   ```
    //
    // For bonus points please implement or outline modifications that would
    // allow to fold (ex.1) to a single filter
    //   `f = createExactFilter("country"); f.add("RU"); f.add("UA");`
    // and to fold (ex.2) to a single filter
    //   `f = createRangeFilter("age"); f.setLowerBound(18, true); f.setUpperBound(24, false);`

}

trait FilterRunner {
    // Queries an external system for items matching the specified filter.
    def query(f: Filter): Seq[Item]
}
