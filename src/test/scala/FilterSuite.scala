import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable.ArrayBuffer

/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class FilterSuite extends FunSuite with FilterBuilder {
    test ("Example 1") {
        val filter = buildFilter("prop[\"country\"] = \"RU\" || prop[\"country\"] = \"UA\"")
        assert(filter.isSuccess === true)
        assert(filter.get.getClass === new OrFilterImpl().getClass)
        val orFilter = filter.get.asInstanceOf[OrFilterImpl]
        assert(orFilter.filters(0) === new ExactFilterImpl("country", "RU"))
        assert(orFilter.filters(1) === new ExactFilterImpl("country", "UA"))
    }

    test ("Example 2") {
        val filter = buildFilter("prop[\"age\"] >= 18 && prop[\"age\"] < 24")
        assert(filter.isSuccess === true)

        assert(filter.get.getClass === new AndFilterImpl().getClass)
        val andFilter = filter.get.asInstanceOf[AndFilterImpl]
        assert(andFilter.filters(0) === new RangeFilterImpl("age", lb = 18, lbInclusive = true))
        assert(andFilter.filters(1) === new RangeFilterImpl("age", ub = 24))
    }

    test ("Example 3") {
        val filter = buildFilter("(prop[\"age\"] <= 18 && prop[\"age\"] < 24) || prop[\"start\"] = \"hello, world\"")
        assert(filter.isSuccess === true)

        assert(filter.get.getClass === new OrFilterImpl().getClass)
        val orFilter = filter.get.asInstanceOf[OrFilterImpl]
        val andFilter = orFilter.filters(0).asInstanceOf[AndFilterImpl]
        assert(andFilter.filters(0) === new RangeFilterImpl("age", ub = 18, ubInclusive = true))
        assert(andFilter.filters(1) === new RangeFilterImpl("age", ub = 24))

        assert(orFilter.filters(1) === new ExactFilterImpl("start", "hello, world"))
    }

    test ("Failure Test") {
        val filter = buildFilter("(prop[\"age\"] <= 18 && prop[\"age\"] < 24 || prop[\"start\"] = \"hello, world\"")
        assert(filter.isSuccess === false)
    }

    test ("Failure Test 2") {
        val filter = buildFilter("() || prop[\"start\"] = \"hello, world\"")
        assert(filter.isSuccess === false)
    }

    test ("Failure Test 3") {
        val filter = buildFilter("(prop[\"age\"] <= 18 && prop[\"age\"] < 24) ||")
        assert(filter.isSuccess === false)
    }
}
