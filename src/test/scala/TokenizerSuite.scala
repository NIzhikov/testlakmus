import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class TokenizerSuite extends FunSuite {
    def toList(t: Tokenizer) = {
        def doToList(t: Tokenizer, acc: List[Token]): List[Token] = {
            val token = t.next
            token match {
                case EOF() => acc :+ token
                case _ => doToList(t, acc :+ token)
            }
        }
        doToList(t, List())
    }

    test ("Example 1 Tokenizer") {
        assert(toList(new Tokenizer("prop[\"country\"] = \"RU\" || prop[\"country\"] = \"UA\"")) ===
            List(PropertyToken("country"), EqualSign(), StringToken("RU"), OrOperator(), PropertyToken("country"),
                EqualSign(), StringToken("UA"), EOF()))
    }

    test ("Example 2 Tokenizer") {
        assert(toList(new Tokenizer("prop[\"age\"] >= 18 && prop[\"age\"] < 24")) ===
            List(PropertyToken("age"), BiggerOrEqualSign(), IntegerToken(18), AndOperator(), PropertyToken("age"),
                SmallerSign(), IntegerToken(24), EOF()))
    }

    test ("Example 1 Tokenizer Without Whitespaces") {
        assert(toList(new Tokenizer("prop[\"country\"]=\"RU\"||prop[\"country\"]=\"UA\"")) ===
            List(PropertyToken("country"), EqualSign(), StringToken("RU"), OrOperator(), PropertyToken("country"),
                EqualSign(), StringToken("UA"), EOF()))
    }

    test ("Example 2 Tokenizer Without Whitespaces") {
        assert(toList(new Tokenizer("prop[\"age\"]>=18&&prop[\"age\"]<24")) ===
            List(PropertyToken("age"), BiggerOrEqualSign(), IntegerToken(18), AndOperator(), PropertyToken("age"),
                SmallerSign(), IntegerToken(24), EOF()))
    }

    test ("Example 3 Tokenizer") {
        assert(toList(new Tokenizer("prop[\"age\"]>=18&&prop[\"age\"]<24 && " +
            "(prop[\"country\"] = \"RU\" || prop[\"country\"] = \"UA\")")) ===
            List(PropertyToken("age"), BiggerOrEqualSign(), IntegerToken(18), AndOperator(), PropertyToken("age"),
                SmallerSign(), IntegerToken(24), AndOperator(), LeftBracket(), PropertyToken("country"),
                EqualSign(), StringToken("RU"), OrOperator(), PropertyToken("country"),
                EqualSign(), StringToken("UA"), RightBracket(), EOF()))
    }
}
